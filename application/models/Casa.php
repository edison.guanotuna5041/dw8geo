<?php
    class Casa extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      // funcion para insertar
      public function insertar($datos){
        return $this->db->insert('casa',$datos);

      }
      // funcion para consultar casa
      public function consultarTodos(){
        $listadoCasas=$this->db->get('casa');
        if ($listadoCasas->num_rows() >0) {
          return $listadoCasas;
          // code...
        } else {
          // code...
          return false;
        }

      }
      


      // funcion eliminar
      public function eliminar($id_cas){
        $this->db->where('id_cas',$id_cas);
        return $this->db->delete('casa');
      }


    }// FINAL DE LA CLASE

 ?>
