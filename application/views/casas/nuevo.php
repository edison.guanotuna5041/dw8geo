
  <form action="<?php echo site_url('');?>/Casas/guardarCasas" method="post">

  <div class="row">
    <div class="col-md-12 mt-2 text-center">
      <h1><b>Nuevo Propietario</b></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3">

    </div>
    <div class="col-md-3 text-center">
        <div class="">
          <input type="text" class="form-control" id="nombre_cas" placeholder="Nombre" name="nombre_cas" minlength="3" maxlength="30" required>
        </div>
        <br>
    </div>
    <div class="col-md-3 text-center">
        <div class="">
          <input type="text" class="form-control" id="apellido_cas" placeholder="Apellido" name="apellido_cas"minlength="3" maxlength="30" required>
        </div>
        <br>
    </div>
    <div class="col-md-3">

    </div>

  </div>
  <!-- parte de imput -->
  <div class="row">
    <!-- ubicacion 1 -->
        <div class="col-md-3">
          <div class="row">
            <div class="col-md-12 mt-4 mb-4  text-center">
              <h1><b>Coordenadas 1</b></h1>
            </div>
          </div>
          <div class="row">

            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lat1_cas" placeholder="Latitud 1" name="lat1_cas" minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lg1_cas" placeholder="Longitud 1" name="lg1_cas"minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
          </div>

        </div>
    <!-- ubicacion 2 -->
        <div class="col-md-3">
          <div class="row">
            <div class="col-md-12 mt-4 mb-4 text-center">
              <h1><b>Coordenadas 2</b></h1>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lat2_cas" placeholder="Latitud 2" name="lat2_cas" minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lg2_cas" placeholder="Longitud 2" name="lg2_cas"minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
          </div>

        </div>
    <!-- ubicacion 3 -->

        <div class="col-md-3">
          <div class="row">
            <div class="col-md-12 mt-4 mb-4  text-center">
              <h1><b>Coordenadas 3</b></h1>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lat3_cas" placeholder="Latitud 3" name="lat3_cas" minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lg3_cas" placeholder="Longitud 3" name="lg3_cas"minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
          </div>
        </div>
    <!-- ubicacion 4 -->
        <div class="col-md-3">
          <div class="row">
            <div class="col-md-12 mt-4 mb-4  text-center">
              <h1><b>Coordenadas 4</b></h1>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lat4_cas" placeholder="Latitud 4" name="lat4_cas" minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
            <div class="col-md-12 text-center">
                <div class="">
                  <input type="text" class="form-control" id="lg4_cas" placeholder="Longitud 4" name="lg4_cas"minlength="3" maxlength="30" required>
                </div>
                <br>
            </div>
          </div>
        </div>
        <!-- fin de cordenadas -->
      </div>
      <!-- fin de imput -->
        <div class="row">
          <div class="col-md-12 mt-4 mb-4 text-center">
            <button type="submit" class="btn btn-primary" style="width:20%"><b>Guardar</b></button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php site_url(''); ?>../Casas/consulta">
            <button type="button" class="btn btn-primary" style="width:20%"><b>Cancelar</b></button>
            </a>
          </div>
        </div>
  </form>
