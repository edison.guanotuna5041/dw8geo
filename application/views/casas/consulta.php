
<?php if ($listadoCasas): ?>
        <div class="container-fluid">
           <div class="row column_title">
              <div class="col-md-12">
                 <div class="page_title text-center">
                    <h2>Tabla de Las Casas </h2>
                 </div>
              </div>
           </div>

        </div>
        <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive pt-3">
              <table class="table table-striped">
                <thead>
                  <tr class="text-center">
                    <th>Id Casa</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Latitudes</th>
                    <th>Longitudes</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($listadoCasas->result() as $filaTemporal): ?>
                    <tr>
                      <td class="text-center">
                      <?php echo $filaTemporal->id_cas;?>
                      </td>
                      <td class="text-center">
                      <?php echo $filaTemporal->nombre_cas;?>
                      </td>
                      <td class="text-center">
                      <?php echo $filaTemporal->apellido_cas;?>
                      </td>
                      <td class="text-center">
                          <li><?php echo $filaTemporal->lat1_cas;?></li>
                          <li><?php echo $filaTemporal->lat2_cas;?></li>
                          <li><?php echo $filaTemporal->lat3_cas;?></li>
                          <li><?php echo $filaTemporal->lat4_cas;?></li>
                      </td>
                      <td class="text-center">
                          <li><?php echo $filaTemporal->lg1_cas;?></li>
                          <li><?php echo $filaTemporal->lg2_cas;?></li>
                          <li><?php echo $filaTemporal->lg3_cas;?></li>
                          <li><?php echo $filaTemporal->lg4_cas;?></li>
                      </td>
                        <td class="text-center">
                        <a href="#"><i class="fa fa-edit"></i> Editar </a>

                        &nbsp;&nbsp;
                        <a onclick="return confirm('Eliminado')" href="<?php echo site_url(''); ?>/casas/procesarEliminacion/<?php echo $filaTemporal->id_cas;?>"><i class="fa fa-minus-circle"></i> Eliminar </a>

                      </td>

                    </tr>

                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No existe Casas</h3>

  </div>

<?php endif; ?>
