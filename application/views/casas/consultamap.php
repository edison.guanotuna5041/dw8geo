<div class="row">
  <div class="col-md-12 text-center mb-5">
    <h1> <b>Mapa De Las Casas</b> </h1>


  </div>

</div>
<?php if ($listadoCasas): ?>
  <div class="row">
    <div id="mapa1" style="width:100%; height:500px;"></div>
        <script type="text/javascript">
        function initMap(){
        alert("MAPA ");
        // Definiendo una coordenada
        var latitud_longitud=new google.maps.LatLng(-0.9309070354351875,-78.60295791348105);
        //Creando el mapa
        var map=new google.maps.Map(document.getElementById('mapa1'),
        {
        center:latitud_longitud,
        zoom:20,
        mapTypeId: google.maps.MapTypeId.HYBRID,        }
         );
         //llamar datos de la base de datos
         <?php foreach ($listadoCasas->result() as $fila): ?>
           var coordenada=[
             { lat:<?php echo $fila->lat1_cas?>, lng: <?php echo $fila->lg1_cas?>},
             { lat:<?php echo $fila->lat2_cas?>, lng: <?php echo $fila->lg2_cas?>},
             { lat:<?php echo $fila->lat3_cas?>, lng:<?php echo $fila->lg3_cas?>},
             { lat:<?php echo $fila->lat4_cas?>, lng:<?php echo $fila->lg4_cas?>},

             ];
             var casa=new google.maps.Polygon({
               path:coordenada,
               strokeColor:"#000000",
               strokeOpacity:"0.5",
               strokeWeight:3,
               fillColor:"#330033",
               fillOpacity:"0.5"

             });
             var marker1= new google.maps.Marker({
             position: new google.maps.LatLng(<?php echo $fila->lat1_cas?>, <?php echo $fila->lg1_cas?>),
             map:map,
             title:"<?php echo $fila->nombre_cas;?> <?php echo $fila->apellido_cas;?>"
             });

             var marker2= new google.maps.Marker({
             position: new google.maps.LatLng(<?php echo $fila->lat2_cas?>, <?php echo $fila->lg2_cas?>),
             map:map,
             title:"<?php echo $fila->nombre_cas;?> <?php echo $fila->apellido_cas;?>"
             });

             var marker3= new google.maps.Marker({
             position: new google.maps.LatLng(<?php echo $fila->lat3_cas?>, <?php echo $fila->lg3_cas?>),
             map:map,
             title:"<?php echo $fila->nombre_cas;?> <?php echo $fila->apellido_cas;?>"
             });

             var marker4= new google.maps.Marker({
             position: new google.maps.LatLng(<?php echo $fila->lat4_cas?>, <?php echo $fila->lg4_cas?>),
             map:map,
             title:"<?php echo $fila->nombre_cas;?> <?php echo $fila->apellido_cas;?>"
             });
             casa.setMap(map);
         //
         <?php endforeach; ?>


        }
        </script>

  </div>




<?php else: ?>
  <h3> no se encontraron ubicaciones registradas</h3>



<?php endif; ?>
