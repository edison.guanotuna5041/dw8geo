<!-- footer -->
          <div class="container-fluid">
             <div class="footer">
                <p>Copyright © 2022 Diseñado por Javier R, Pamela S y Kevin T. Reservados todos los derechos.<br><br>
                   Distribuido por: <a target="_blank" href="https://www.facebook.com/javier.R.332306/">Facebook</a>
                </p>
             </div>
          </div>
      </div>
    <!-- end dashboard inner -->
    </div>
  </div>
</div>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
<!-- wow animation -->
<script src="<?php echo base_url(); ?>/assets/js/animate.js"></script>
<!-- select country -->
<script src="<?php echo base_url(); ?>/assets/js/bootstrap-select.js"></script>
<!-- owl carousel -->
<script src="<?php echo base_url(); ?>/assets/js/owl.carousel.js"></script>
<!-- chart js -->
<script src="<?php echo base_url(); ?>/assets/js/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/Chart.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/utils.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/analyser.js"></script>
<!-- nice scrollbar -->
<script src="<?php echo base_url(); ?>/assets/js/perfect-scrollbar.min.js"></script>
<script>
var ps = new PerfectScrollbar('#sidebar');
</script>
<!-- custom js -->
<script src="<?php echo base_url(); ?>/assets/js/custom.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/chart_custom_style1.js"></script>

</body>
</html>
