
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Geo Referencia</title>
      <!-- site icon -->
      <!-- bootstrap css -->
      <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap.min.css" />
      <!-- site css -->
      <link rel="stylesheet" href="<?php echo base_url();?>/assets/style.css" />
      <!-- responsive css -->
      <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/responsive.css" />
      <!-- select bootstrap -->
      <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap-select.css" />
      <!-- scrollbar css -->
      <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/perfect-scrollbar.css" />
      <!-- custom css -->
      <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/custom.css" />
          <!-- API -->
          <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA16p-nrI322KhyV-hsSvtq6dRLdbmhVZw&libraries=places&callback=initMap"></script>

   </head>
   <body class="dashboard dashboard_1">
      <div class="full_container">
         <div class="inner_container">
            <!-- Sidebar  -->
            <nav id="sidebar">
               <div class="sidebar_blog_1">
                  <div class="sidebar-header">
                     <div class="logo_section">
                        <a href="<?php echo site_url('') ?>"><img class="logo_icon img-responsive" src="<?php echo base_url() ?>/assets/images/logo/logo_icon.png" alt="#" /></a>
                     </div>
                  </div>
                  <div class="sidebar_user_info">
                     <div class="icon_setting"></div>
                     <div class="user_profle_side">
                        <div class="user_img"><img class="img-responsive" src="<?php echo base_url(); ?>/assets/images/layout_img/user_img.jpg" alt="" /></div>
                        <div class="user_info">
                           <h6>Grupal 3</h6>
                           <p><span class="online_animation"></span> Online</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="sidebar_blog_2">
                  <h4>General</h4>
                  <ul class="list-unstyled components">
                     <li class="active">
                     </li>
                     <li>
                        <a href="#localidad" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-database purple_color"></i> <span>Localidad</span></a>
                        <ul class="collapse list-unstyled" id="localidad">
                           <li><a href="<?php echo site_url('') ?>/casas/nuevo">> <span>Añadir</span></a></li>
                           <li><a href="<?php echo site_url('') ?>/casas/consulta">> <span>Visualizar</span></a></li>

                           <li><a href="<?php echo site_url('') ?>/casas/consultamap">> <span>Mapa</span></a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </nav>
            <!-- end sidebar -->
            <!-- right content -->
            <div id="content">
               <!-- topbar -->
               <div class="topbar">
                  <nav class="navbar navbar-expand-lg navbar-light">
                     <div class="full">
                        <button type="button" id="sidebarCollapse" class="sidebar_toggle"><i class="fa fa-bars"></i></button>
                        <div class="logo_section">
                           <a href="<?php echo site_url('') ?>"><img class="img-responsive" src="<?php echo base_url();?>/assets/images/logo/logo.png" alt="#" /></a>
                        </div>
                        <div class="right_topbar">
                           <div class="icon_info">
                              <ul>
                                 <li><a href="#"><i class="fa fa-bell-o"></i><span class="badge"></span></a></li>
                                 <li><a href="#"><i class="fa fa-question-circle"></i></a></li>
                                 <li><a href="#"><i class="fa fa-envelope-o"></i><span class="badge"></span></a></li>
                              </ul>
                              <ul class="user_profile_dd">
                                 <li>
                                    <a class="dropdown-toggle" data-toggle="dropdown"><img class="img-responsive rounded-circle" src="<?php echo base_url();?>/assets/images/layout_img/user_img.jpg" alt="#" /><span class="name_user">UTC</span></a>
                                    <div class="dropdown-menu">
                                       <a class="dropdown-item" href="profile.html">Mi Perfil</a>
                                       <a class="dropdown-item" href="settings.html">Configuracion</a>
                                       <a class="dropdown-item" href="help.html">Ayuda</a>
                                       <a class="dropdown-item" href="#"><span>Salir</span> <i class="fa fa-sign-out"></i></a>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
               <!-- end topbar -->
               <!-- dashboard inner -->
               <div class="midde_cont">
                  <div class="container-fluid">
                     <div class="row column_title">
                        <div class="col-md-12">
                           <div class="page_title">
                              <h2>Bienvenido </h2>
                           </div>
                        </div>
                     </div>

                  </div>
