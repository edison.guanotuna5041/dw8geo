<?php
  class Casas extends CI_Controller{
    public function __construct(){
      parent::__construct();
      $this->load->model("casa");
    }
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("casas/nuevo");
      $this->load->view("footer");
    }
    // consultar en tabla
    public function consulta(){
      $this->load->view("header");
      $data["listadoCasas"]=$this->casa->consultarTodos();
      $this->load->view("casas/consulta",$data);
      $this->load->view("footer");
    }
    // consultar en mapa de google

    public function consultamap(){
      $this->load->view("header");
      $data["listadoCasas"]=$this->casa->consultarTodos();
      $this->load->view("casas/consultamap",$data);
      $this->load->view("footer");

    }
    //guardar en la base de datos
    public function guardarCasas() {
      $datosNuevoCasas=array(
        "nombre_cas"=>$this->input->post("nombre_cas"),
        "apellido_cas"=>$this->input->post("apellido_cas"),
        "lat1_cas"=>$this->input->post("lat1_cas"),
        "lg1_cas"=>$this->input->post("lg1_cas"),
        "lat2_cas"=>$this->input->post("lat2_cas"),
        "lg2_cas"=>$this->input->post("lg2_cas"),
        "lat3_cas"=>$this->input->post("lat3_cas"),
        "lg3_cas"=>$this->input->post("lg3_cas"),
        "lat4_cas"=>$this->input->post("lat4_cas"),
        "lg4_cas"=>$this->input->post("lg4_cas")
      );
      if ($this->casa->insertar($datosNuevoCasas)) {
        redirect("casas/consulta");
        // code...
      } else {
        echo "Error";
        // code...
      }

    }

    // eliminar de la base de datos controler vista
    public function procesarELiminacion($id_cas){
      if ($this->casa->eliminar($id_cas)) {
        // code...
        redirect("casas/consulta");
      } else {
        echo "No se elimino";
        // code...
      }

    }


  } // cierre del controlador casas
 ?>
